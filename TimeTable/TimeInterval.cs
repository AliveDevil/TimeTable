﻿namespace TimeTable
{
	public enum TimeInterval
	{
		Day,
		Week,
		Month
	}
}
