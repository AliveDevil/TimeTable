﻿using System.Collections.Generic;

namespace TimeTable.Model
{
	public class Project
	{
		public int Id { get; set; }

		public bool May { get; set; }

		public string Name { get; set; }

		public ICollection<Record> Records { get; set; }
	}
}
