﻿using System;
using System.Windows.Input;

namespace TimeTable
{
    public class RelayCommand : ICommand, IDisposable
    {
        public event EventHandler CanExecuteChanged;

        private Func<bool> canExecuteFunc;
        private Action executeAction;

        public RelayCommand(Action executeAction) : this(executeAction, () => true)
        {
        }

        public RelayCommand(Action executeAction, Func<bool> canExecuteFunc)
        {
            this.executeAction = executeAction;
            this.canExecuteFunc = canExecuteFunc;
        }

        public bool CanExecute(object parameter)
        {
            return canExecuteFunc();
        }

        public void Dispose()
        {
            RemoveAllEvents();
        }

        public void Execute(object parameter)
        {
            executeAction();
        }

        public void RemoveAllEvents()
        {
            CanExecuteChanged = null;
        }
    }

    public class RelayCommand<T> : ICommand, IDisposable
    {
        public event EventHandler CanExecuteChanged;

        private Func<T, bool> canExecuteFunc;
        private Action<T> executeAction;

        public RelayCommand(Action<T> executeAction) : this(executeAction, (ignore) => true)
        {
        }

        public RelayCommand(Action<T> executeAction, Func<T, bool> canExecuteFunc)
        {
            this.executeAction = executeAction;
            this.canExecuteFunc = canExecuteFunc;
        }

        public bool CanExecute(object parameter)
        {
            if (!(parameter is T))
                return false;
            return canExecuteFunc((T)parameter);
        }

        public void Dispose()
        {
            RemoveAllEvents();
        }

        public void Execute(object parameter)
        {
            if (!(parameter is T))
                return;
            executeAction((T)parameter);
        }

        public void RemoveAllEvents()
        {
            CanExecuteChanged = null;
        }
    }
}
