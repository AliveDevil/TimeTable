# TimeTable

Management App for freelancer and students having restricted work time per week.  
Features:
- Per Project Tracking (Date + Time)
- Aggregation of Time (Per Project, Cross Project)
    - Respecting maximum work time per configurable time interval
        - Week
        - Month
- Display of hours to be put on invoices
    - Charted
    - Tabled with

| Configurable Time Interval | Project 1 Total Hours (Capped) | Project 2 Total Hours (Capped) | … | Total (Capped) |
| --- | --- | --- | --- | --- |
| Week 01.01.17 - 08.01.17 | 10 (10) | 10 (10) | … | 20 (20) |
| … | … | … | … | … |
| Week 2 23.01.17 - 31.01.17 | 5 (0) | 20 (20) | … | 25 (20) |

- Projects may be declared as "overtime" allowing worked hours to be taken over to next configured time interval
