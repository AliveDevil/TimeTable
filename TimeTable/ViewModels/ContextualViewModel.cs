﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;
using TimeTable.Interfaces;

namespace TimeTable.ViewModels
{
	public class ContextualViewModel : INotifyPropertyChanged, IDisposable
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private Lazy<IServiceScope> serviceScope = new Lazy<IServiceScope>(() => App.Current.ServiceProvider.CreateScope());

		public IDbContext Context => ServiceProvider.GetService<IDbContext>();

		public IServiceProvider ServiceProvider => serviceScope.Value.ServiceProvider;

		public void Dispose()
		{
			if (serviceScope.IsValueCreated)
			{
				serviceScope.Value.Dispose();
			}
		}

		protected void OnPropertyChanged([CallerMemberName]string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		protected bool RaiseSetIfChanged<T>(ref T field, T value, [CallerMemberName]string propertyName = null)
		{
			if (Equals(field, value))
				return false;

			field = value;
			OnPropertyChanged(propertyName);
			return true;
		}
	}
}
