﻿using Microsoft.EntityFrameworkCore;
using TimeTable.Interfaces;
using TimeTable.Model;

namespace TimeTable.Services
{
	public class Context : DbContext, IDbContext
	{
		public DbSet<Project> Projects { get; set; }

		public DbSet<Record> Records { get; set; }
	}
}
