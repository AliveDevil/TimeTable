﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TimeTable.Interfaces;
using TimeTable.Services;

namespace TimeTable
{
	public static class Configuration
	{
		public static void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<Context>((serviceProvider, optionsBuilder) =>
			{
				optionsBuilder.UseSqlite("Filename=Table.db");
			});
			services.AddScoped<IDbContext>(serviceProvider => serviceProvider.GetService<Context>());
		}
	}
}
