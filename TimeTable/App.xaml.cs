﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace TimeTable
{
	/// <summary>
	/// Stellt das anwendungsspezifische Verhalten bereit, um die Standardanwendungsklasse zu ergänzen.
	/// </summary>
	sealed partial class App : Application
	{
		public static new App Current => (App)Application.Current;

		public IServiceProvider ServiceProvider { get; private set; }

		/// <summary>
		/// Initialisiert das Singletonanwendungsobjekt. Dies ist die erste Zeile von erstelltem Code
		/// und daher das logische Äquivalent von main() bzw. WinMain().
		/// </summary>
		public App()
		{
			InitializeComponent();
			Suspending += OnSuspending;
		}

		public static void SetLocalSetting<T>(string key, T value, bool overwrite) => SetSetting(ApplicationData.Current.LocalSettings, key, value, overwrite);

		public static void SetRoamingSetting<T>(string key, T value, bool overwrite) => SetSetting(ApplicationData.Current.RoamingSettings, key, value, overwrite);

		public static void SetSetting<T>(ApplicationDataContainer container, string key, T value, bool overwrite)
		{
			if (!container.Values.ContainsKey(key) || overwrite)
				container.Values[key] = value;
		}

		/// <summary>
		/// Wird aufgerufen, wenn die Anwendung durch den Endbenutzer normal gestartet wird. Weitere Einstiegspunkte
		/// werden z. B. verwendet, wenn die Anwendung gestartet wird, um eine bestimmte Datei zu öffnen.
		/// </summary>
		/// <param name="e">Details über Startanforderung und -prozess.</param>
		protected override void OnLaunched(LaunchActivatedEventArgs e)
		{
#if DEBUG
			if (System.Diagnostics.Debugger.IsAttached)
			{
				DebugSettings.EnableFrameRateCounter = true;
			}
#endif

			SetRoamingSetting("Interval", TimeInterval.Week.ToString(), false);
			SetRoamingSetting("MaxIntervalHours", 20, false);

			if (ServiceProvider == null)
			{
				IServiceCollection services = new ServiceCollection();
				Configuration.ConfigureServices(services);
				ServiceProvider = services.BuildServiceProvider();
			}

			Frame rootFrame = Window.Current.Content as Frame;

			// App-Initialisierung nicht wiederholen, wenn das Fenster bereits Inhalte enthält.
			// Nur sicherstellen, dass das Fenster aktiv ist.
			if (rootFrame == null)
			{
				// Frame erstellen, der als Navigationskontext fungiert und zum Parameter der ersten Seite navigieren
				rootFrame = new Frame();

				rootFrame.NavigationFailed += OnNavigationFailed;

				if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
				{
					//TODO: Zustand von zuvor angehaltener Anwendung laden
				}

				// Den Frame im aktuellen Fenster platzieren
				Window.Current.Content = rootFrame;
			}

			if (e.PrelaunchActivated == false)
			{
				if (rootFrame.Content == null)
				{
					// Wenn der Navigationsstapel nicht wiederhergestellt wird, zur ersten Seite navigieren
					// und die neue Seite konfigurieren, indem die erforderlichen Informationen als Navigationsparameter
					// übergeben werden
					rootFrame.Navigate(typeof(MainPage), e.Arguments);
				}
				// Sicherstellen, dass das aktuelle Fenster aktiv ist
				Window.Current.Activate();
			}
		}

		/// <summary>
		/// Wird aufgerufen, wenn die Navigation auf eine bestimmte Seite fehlschlägt
		/// </summary>
		/// <param name="sender">Der Rahmen, bei dem die Navigation fehlgeschlagen ist</param>
		/// <param name="e">Details über den Navigationsfehler</param>
		private void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
		{
			throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
		}

		/// <summary>
		/// Wird aufgerufen, wenn die Ausführung der Anwendung angehalten wird.  Der Anwendungszustand wird gespeichert,
		/// ohne zu wissen, ob die Anwendung beendet oder fortgesetzt wird und die Speicherinhalte dabei
		/// unbeschädigt bleiben.
		/// </summary>
		/// <param name="sender">Die Quelle der Anhalteanforderung.</param>
		/// <param name="e">Details zur Anhalteanforderung.</param>
		private void OnSuspending(object sender, SuspendingEventArgs e)
		{
			var deferral = e.SuspendingOperation.GetDeferral();

			//TODO: Anwendungszustand speichern und alle Hintergrundaktivitäten beenden
			deferral.Complete();
		}
	}
}
