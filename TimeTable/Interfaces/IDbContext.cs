﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TimeTable.Model;

namespace TimeTable.Interfaces
{
	public interface IDbContext
	{
		DbSet<Project> Projects { get; set; }

		DbSet<Record> Records { get; set; }

		int SaveChanges();

		Task<int> SaveChangesAsync(CancellationToken cancellationToken);
	}
}
