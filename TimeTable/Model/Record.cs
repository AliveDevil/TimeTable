﻿using System;

namespace TimeTable.Model
{
	public class Record
	{
		public DateTimeOffset Date { get; set; }

		public int Id { get; set; }

		public Project Project { get; set; }

		public TimeSpan Time { get; set; }
	}
}
